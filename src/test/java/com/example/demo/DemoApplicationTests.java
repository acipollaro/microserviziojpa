package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Autowired
	private AnagraficaRepository repository;
	
	private static final Logger log = LoggerFactory.getLogger(DemoApplicationTests.class);

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void getNome() {
		String nome = "Antonio";
		// fetch customers by last name
		log.info("Customer found with findByNome('Antonio'):");
		log.info("--------------------------------------------");
		repository.findByNome(nome).forEach(name -> {
			log.info(name.getNome() +" "+name.getCognome());
		});
		// for (Customer bauer : repository.findByLastName("Bauer")) {
		// 	log.info(bauer.toString());
		// }
		log.info("");
	}

}
