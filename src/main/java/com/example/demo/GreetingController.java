package com.example.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    
    @Autowired
	private AnagraficaRepository repository;

    @RequestMapping("/saluti")
    public Saluto greeting(@RequestParam(value="name", defaultValue="World") String name) {
       
    	String nome = "Antonio";
		
		
		Anagrafica a = repository.findByNome(nome).get(0);
		
		
    	if(null == name || "".equals(name) || null == a)
        	return new Saluto(String.format(template, name));
        else
        	return new Saluto(a.getNome() + " "+a.getCognome());
        	
    }
}
