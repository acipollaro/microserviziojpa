package com.example.demo;


import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface AnagraficaRepository extends CrudRepository<Anagrafica, Long> {

    List<Anagrafica> findByNome(String Nome);
}