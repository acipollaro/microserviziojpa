package com.example.demo;

public class Saluto {

	private String value;
	
	public Saluto(String saluto) {
		setValue(saluto);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
