package com.example.demo;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the anagrafica database table.
 * 
 */
@Entity
@NamedQuery(name="Anagrafica.findAll", query="SELECT a FROM Anagrafica a")
public class Anagrafica implements Serializable {
	private static final long serialVersionUID = 1L;

	private String cognome;

	@Id
	private Long id;

	private String nome;

	public Anagrafica() {
	}

	public String getCognome() {
		return this.cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}